---
title: CentOS 开发环境搭建
date: 2018-09-07 09:25:00
author: Ryanjie
img: https://wx3.sinaimg.cn/large/006pYIPbly1g1rrzuhwqjj325s0yie85.jpg
top: false
cover: false
toc: true
mathjax: false
summary: CentOS开发环境搭建: JDK, Tomcat, Maven环境搭建。
categories: CentOS
tags:
  - CentOS
---



# 0x01. 安装配置 JDK

```shell
# 卸载之前的JDK
## 查询系统是否安装 jdk
rpm -qa|grep java
rpm -qa|grep jdk
## 卸载已经安装的jdk
rpm -e --nodeps {package-name}
## 验证是否还存在jdk
java -version
javac -version

# 下载 JDK
## 下载 JDK 到 /opt 目录
sudo wget https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/x86_64/jdk8-8u201-1-x86_64.pkg.tar.xz -P /opt/
## 解压安装下载的 JDK 包
sudo xz -d /opt/jdk8-8u201-1-x86_64.pkg.tar.xz && sudo tar -xvf /opt/jdk8-8u201-1-x86_64.pkg.tar -C /
## 创建软连接jdk8
cd /usr/lib/jvm/ && sudo ln -s java-8-jdk jdk8
## 删除之前下载的jdk包
sudo rm -rf /opt/jdk8-8u201-1-x86_64.pkg.tar

# 配置环境变量
cd /etc/profile.d/ && sudo touch java8.sh && sudo chmod 747 java8.sh && vim java8.sh
# CentOS Java8 Env

export JAVA_HOME=/usr/lib/jvm/jdk8
export CLASS_PATH=.:$JAVA_HOME/lib
export PATH=$PATH:$JAVA_HOME/bin
  
# 使环境变量立即生效  
source /etc/profile

# 验证环境变量配置成功
javac -version
java -version
```

命令演示

``` shell
[ryanjie@Ryanjie ~]$ sudo wget https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/x86_64/jdk8-8u201-1-x86_64.pkg.tar.xz -P /opt/
--2019-04-16 15:44:11--  https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/x86_64/jdk8-8u201-1-x86_64.pkg.tar.xz
Resolving mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)... 2402:f000:1:408:8100::1, 101.6.8.193
Connecting to mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)|2402:f000:1:408:8100::1|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 156079644 (149M) [application/x-xz]
Saving to: ‘/opt/jdk8-8u201-1-x86_64.pkg.tar.xz’

100%[===========================================>] 156,079,644 4.10MB/s   in 33s    

2019-04-16 15:44:44 (4.54 MB/s) - ‘/opt/jdk8-8u201-1-x86_64.pkg.tar.xz’ saved [156079644/156079644]

[ryanjie@Ryanjie ~]$ 
[ryanjie@Ryanjie ~]$ sudo xz -d /opt/jdk8-8u201-1-x86_64.pkg.tar.xz && sudo tar -xvf /opt/jdk8-8u201-1-x86_64.pkg.tar -C /
.MTREE
.INSTALL
.BUILDINFO
.PKGINFO
etc/
etc/.java/
etc/.java/.systemPrefs/
etc/java-jdk8/
etc/java-jdk8/amd64/
etc/java-jdk8/amd64/jvm.cfg
......
......
......
usr/share/man/man1/rmiregistry-jdk8.1.gz
usr/share/man/man1/schemagen-jdk8.1.gz
usr/share/man/man1/serialver-jdk8.1.gz
usr/share/man/man1/servertool-jdk8.1.gz
usr/share/man/man1/tnameserv-jdk8.1.gz
usr/share/man/man1/unpack200-jdk8.1.gz
usr/share/man/man1/wsgen-jdk8.1.gz
usr/share/man/man1/wsimport-jdk8.1.gz
usr/share/man/man1/xjc-jdk8.1.gz
usr/share/mime/
usr/share/mime/packages/
usr/share/mime/packages/x-java-archive-jdk8.xml
usr/share/mime/packages/x-java-jnlp-file-jdk8.xml
[ryanjie@Ryanjie ~]$ cd /etc/profile.d/ && sudo touch java8.sh && sudo chmod 747 java8.sh && vim java8.sh
[ryanjie@Ryanjie profile.d]$ source /etc/profile
[ryanjie@Ryanjie profile.d]$ javac -version
javac 1.8.0_201
[ryanjie@Ryanjie profile.d]$ java -version
java version "1.8.0_201"
Java(TM) SE Runtime Environment (build 1.8.0_201-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)
[ryanjie@Ryanjie profile.d]$

```

# 0x02. 安装配置 Tomcat


```shell
# 下载 Tomcat
## 下载 tomcat 包 
sudo wget https://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.40/bin/apache-tomcat-8.5.40.tar.gz -P /opt/
## 解压刚才下载的 Tomcat 包
sudo tar -zxvf /opt/apache-tomcat-8.5.40.tar.gz -C /opt
## 创建软连接 tomcat8
cd /opt/ && sudo ln -s apache-tomcat-8.5.40 tomcat8
## 删除之前下载的tomcat包
sudo rm -rf apache-tomcat-8.5.40.tar.gz 

# 配置 Tomcat
cd /etc/profile.d/ && sudo touch tomcat8.sh && sudo chmod 747 tomcat8.sh && vim tomcat8.sh

# CentOS Tomcat8 Env

export CATALINA_HOME=/opt/tomcat8
export CATALINA_BASE=/opt/tomcat8
export PATH=$PATH:$CATLINA_HOME/bin

# 使环境变量立即生效  
source /etc/profile
```

命令过程

``` shell
[ryanjie@Ryanjie ~]$ sudo wget https://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.40/bin/apache-tomcat-8.5.40.tar.gz -P /opt/
--2019-04-16 11:08:20--  https://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.40/bin/apache-tomcat-8.5.40.tar.gz
Resolving mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)... 2402:f000:1:408:8100::1, 101.6.8.193
Connecting to mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)|2402:f000:1:408:8100::1|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 9690027 (9.2M) [application/x-gzip]
Saving to: ‘/opt/apache-tomcat-8.5.40.tar.gz’

100%[======================================>] 9,690,027   5.70MB/s   in 1.6s   

2019-04-16 11:08:21 (5.70 MB/s) - ‘/opt/apache-tomcat-8.5.40.tar.gz’ saved [9690027/9690027]

[ryanjie@Ryanjie ~]$
[ryanjie@Ryanjie ~]$ sudo tar -zxvf /opt/apache-tomcat-8.5.40.tar.gz -C /opt
apache-tomcat-8.5.40/conf/
apache-tomcat-8.5.40/conf/catalina.policy
apache-tomcat-8.5.40/conf/catalina.properties
apache-tomcat-8.5.40/conf/context.xml
apache-tomcat-8.5.40/conf/jaspic-providers.xml
apache-tomcat-8.5.40/conf/jaspic-providers.xsd
apache-tomcat-8.5.40/conf/logging.properties
apache-tomcat-8.5.40/conf/server.xml
......
......
......
apache-tomcat-8.5.40/bin/catalina.sh
apache-tomcat-8.5.40/bin/configtest.sh
apache-tomcat-8.5.40/bin/daemon.sh
apache-tomcat-8.5.40/bin/digest.sh
apache-tomcat-8.5.40/bin/setclasspath.sh
apache-tomcat-8.5.40/bin/shutdown.sh
apache-tomcat-8.5.40/bin/startup.sh
apache-tomcat-8.5.40/bin/tool-wrapper.sh
apache-tomcat-8.5.40/bin/version.sh
[ryanjie@Ryanjie ~]$ 
[ryanjie@Ryanjie ~]$ cd /opt 
[ryanjie@Ryanjie opt]$ sudo ln -s apache-tomcat-8.5.40 tomcat8
[ryanjie@Ryanjie opt]$ sudo rm -rf apache-tomcat-8.5.40.tar.gz 

## 配置 Tomcat
[ryanjie@Ryanjie opt]$ sudo touch /etc/profile.d/tomcat8.sh

```

# 0x03. 安装配置 Maven

```shell
# 下载maven包
sudo wget https://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz -P /opt
# 解压下载的maven包
sudo tar -zxvf /opt/apache-maven-3.6.0-bin.tar.gz -C /opt
# 创建软连接以及修改权限
cd /opt && sudo ln -s apache-maven-3.6.0 maven3 && sudo chmod -R 717 maven3

# 配置 Maven
cd /etc/profile.d/ && sudo touch maven3.sh && sudo chmod 747 maven3.sh && vim maven3.sh

# CentOS Maven3 Env

export M2_HOME/opt/maven3
export PATH=$PATH:$M2_HOME/bin

# 使环境变量立即生效  
source /etc/profile

# 验证Maven是否配置成功
mvn -v
```

命令演示

``` shell
[ryanjie@Ryanjie ~]$ sudo wget https://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.tar.gz -P /opt
--2019-04-16 16:56:18--  https://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.tar.gz
Resolving mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)... 2402:f000:1:408:8100::1, 101.6.8.193
Connecting to mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)|2402:f000:1:408:8100::1|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 9136463 (8.7M) [application/x-gzip]
Saving to: ‘/opt/apache-maven-3.6.1-bin.tar.gz’

100%[===========================================>] 9,136,463   6.09MB/s   in 1.4s   

2019-04-16 16:56:20 (6.09 MB/s) - ‘/opt/apache-maven-3.6.1-bin.tar.gz’ saved [9136463/9136463]

[ryanjie@Ryanjie ~]$ 
[ryanjie@Ryanjie ~]$ sudo tar -zxvf /opt/apache-maven-3.6.1-bin.tar.gz -C /opt
apache-maven-3.6.1/README.txt
apache-maven-3.6.1/LICENSE
apache-maven-3.6.1/NOTICE
apache-maven-3.6.1/lib/
apache-maven-3.6.1/lib/slf4j-api.license
apache-maven-3.6.1/lib/checker-compat-qual.license
apache-maven-3.6.1/lib/jsr250-api.license
apache-maven-3.6.1/lib/jcl-over-slf4j.license
apache-maven-3.6.1/lib/org.eclipse.sisu.plexus.license
apache-maven-3.6.1/lib/animal-sniffer-annotations.license
apache-maven-3.6.1/lib/org.eclipse.sisu.inject.license
......
......
......
apache-maven-3.6.1/lib/commons-cli-1.4.jar
apache-maven-3.6.1/lib/maven-compat-3.6.1.jar
apache-maven-3.6.1/lib/wagon-provider-api-3.3.2.jar
apache-maven-3.6.1/lib/wagon-http-3.3.2-shaded.jar
apache-maven-3.6.1/lib/jcl-over-slf4j-1.7.25.jar
apache-maven-3.6.1/lib/wagon-file-3.3.2.jar
apache-maven-3.6.1/lib/maven-resolver-connector-basic-1.3.3.jar
apache-maven-3.6.1/lib/maven-resolver-transport-wagon-1.3.3.jar
apache-maven-3.6.1/lib/maven-slf4j-provider-3.6.1.jar
apache-maven-3.6.1/lib/jansi-1.17.1.jar
[ryanjie@Ryanjie ~]$ cd /opt && sudo ln -s apache-maven-3.6.1 maven3 && sudo chmod -R 717 maven3
[ryanjie@Ryanjie ~]$ mvn -v
Apache Maven 3.6.1 (d66c9c0b3152b2e69ee9bac180bb8fcc8e6af555; 2019-04-05T03:00:29+08:00)
Maven home: /opt/maven3
Java version: 1.8.0_201, vendor: Oracle Corporation, runtime: /usr/lib/jvm/java-8-jdk/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "3.10.0-957.10.1.el7.x86_64", arch: "amd64", family: "unix"
[ryanjie@Ryanjie ~]$ 
```



-----------------

> **版权声明：** 本博客所有文章除特别声明外，均采用 **[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)** 许可协议。
> 
> **作者**：[@Ryanjie](https://www.ryanjie.xyz)  ||  [@Ryanjie's Cnblogs](https://www.cnblogs.com/Ryanjie)  ||  [@Ryanjie's Github](https://github.com/ryanjiena)  ||  [@Ryanjie's Gitee](https://gitee.com/ryanjiena)
> 
>  **转载请注明出处：**[@Ryanjie](https://www.ryanjie.xyz)

------
#!/usr/bin/env bash
#
# 显示内核版本
echo ""
echo "= = = = = = = = = Docker 要求 CentOS 系统的内核版本高于 3.10,下面来验证你的CentOS 版本是否支持 Docker = = = = = = = = ="
echo ""
uname -r

# 卸载旧版本 Uninstall old version
echo ""
echo "= = = = = = = = = 卸载旧版本 Uninstall old version = = = = = = = = ="
echo ""
sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine

# 安装依赖包 Install required packages(um-util 提供yum-config-manager功能，另外两个是devicemapper驱动依赖的)
echo ""
echo "= = = = = = = = = 安装依赖包 Install required packages = = = = = = = = ="
echo ""
sudo yum -y install yum-utils device-mapper-persistent-data lvm2

# 设置软件yum源
echo ""
echo "= = = = = = = = = 设置软件yum源 = = = = = = = = ="
echo ""
wget -O /opt/docker-ce.repo https://raw.githubusercontent.com/Ryanjiena/CentOS_Repo/master/Docker/docker-ce.repo
sudo yum-config-manager --add-repo /opt/docker-ce.repo

# 更新yum缓存
echo ""
echo "= = = = = = = = = 更新yum缓存 = = = = = = = = ="
echo ""
sudo yum makecache fast

# 查看所有仓库中所有docker版本，并选择特定版本安装
echo ""
echo "= = = = = = = = = 查看所有仓库中所有docker版本，并选择特定版本安装 = = = = = = = = ="
echo ""
sudo yum list docker-ce --showduplicates | sort -r

# 安装 docker-ce docker-ce-cli containerd.io
echo ""
echo "= = = = = = = = = 安装 docker-ce docker-ce-cli containerd.io = = = = = = = = ="
echo ""
sudo yum -y install docker-ce docker-ce-cli containerd.io

# 启动Docker Start Docker
echo ""
echo "= = = = = = = = = 启动Docker Start Docker = = = = = = = = ="
echo ""
sudo systemctl start docker

# 验证安装是否成功
echo ""
echo "= = = = = = = = = 验证安装是否成功 = = = = = = = = ="
echo ""
docker version

# 卸载Docker
# sudo yum -y remove docker-ce docker-ce-cli containerd.io && sudo rm -rf /var/lib/docker
# sudo yum -y remove yum-utils device-mapper-persistent-data lvm2
---
title: CentOS 安装 Docker
date: 2018-09-07 09:25:00
author: Ryanjie
img: https://wx3.sinaimg.cn/large/006pYIPbly1g1rrzuhwqjj325s0yie85.jpg
top: false
cover: false
toc: true
mathjax: false
summary: CentOS 7.8 安装 Docker 步骤以及 Docker 官方镜像无法访问问题的解决方法.
categories: CentOS 
tags:
  - CentOS
  - Docker
---

**[官方文档](https://docs.docker.com/install/linux/docker-ce/centos/)**

<!-- TOC -->

- [0x01. 问题描述](#0x01-问题描述)
- [0x02. 解决办法](#0x02-解决办法)
- [0x03. 脚本 Getdocker.sh](#0x03-脚本-getdockersh)
      - [3.1 脚本代码](#31-脚本代码)
      - [3.2 执行结果](#32-执行结果)

<!-- /TOC -->

## 0x01. 问题描述

由于 `CentOS` 在安装 `Docker` 时,经常会在 `yum-config-manager --add-repo https://download.docker.com.linux/centos/docker-ce.repo`这一步总会报以下错误,自己花了很长时间才找到这个原因.解决办法在下面.

```bash
Could not fetch/save url https://download.docker.com.linux/centos/docker-ce.repo to file /etc/yum.repos.d/docker-ce.repo: [Errno 14] curl#7 - "Failed connect to download.docker.com.linux:443; Connection refused"

https://download-stage.docker.com/linux/centos/7/x86_64/stable/repodata/repomd.xml:
 [Errno 12] Timeout on https://download-stage.docker.com/linux/centos/7/x86_64/stable/repodata/repomd.xml:
 (28, 'Operation timed out after 30001 milliseconds with 0 out of 0 bytes received')
Trying other mirror. 
```

## 0x02. 解决办法

 1. `uname -r`: Docker 要求 CentOS 系统的内核版本高于 3.10,下面来验证你的CentOS 版本是否支持 Docker.
 2. `sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine` : 卸载旧版本 `Uninstall old version`
 3. `sudo yum -y install yum-utils device-mapper-persistent-data lvm2`: 安装依赖包 `Install required packages`(`um-util` 提供`yum-config-manager`功能，另外两个是`devicemapper`驱动依赖的).
 4. `wget -O /etc/yum.repos.d/docker-ce.repo https://download.docker.com/linux/centos/docker-ce.repo`: 下载官方的`repo`文件.
 5. `sudo sed -i 's+download.docker.com+mirrors.tuna.tsinghua.edu.cn/docker-ce+' /etc/yum.repos.d/docker-ce.repo`: 在刚才下载的`repo`文件中把软件仓库地址替换为 `TUNA`(清华镜像下载站)
 6. `sudo yum makecache fast`: 更新`yum`缓存.
 7. `sudo yum -y install docker-ce docker-ce-cli containerd.io`: 安装 `docker-ce docker-ce-cli containerd.io`.
 8. `sudo systemctl start docker`: 启动 `Docker`.
 9. `docker version`: 查看 `Docker` 版本.
 

## 0x03. 脚本 Getdocker.sh

**使用方法: 下载脚本添加可执行权限之后运行就好**

``` shell
 chmod +x GetDocker.sh && sudo sh GetDocker.sh
```

### 3.1 脚本代码
// Getdocker.sh 

```shell
#!/usr/bin/env bash
#
# 显示内核版本
echo ""
echo "= = = = = = = = = Docker 要求 CentOS 系统的内核版本高于 3.10,下面来验证你的CentOS 版本是否支持 Docker = = = = = = = = ="
echo ""
uname -r

# 卸载旧版本 Uninstall old version
echo ""
echo "= = = = = = = = = 卸载旧版本 Uninstall old version = = = = = = = = ="
echo ""
sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine

# 安装依赖包 Install required packages(um-util 提供yum-config-manager功能，另外两个是devicemapper驱动依赖的)
echo ""
echo "= = = = = = = = = 安装依赖包 Install required packages = = = = = = = = ="
echo ""
sudo yum -y install yum-utils device-mapper-persistent-data lvm2

# 设置软件yum源
echo ""
echo "= = = = = = = = = 设置软件yum源 = = = = = = = = ="
echo ""
wget -O /opt/docker-ce.repo https://raw.githubusercontent.com/Ryanjiena/CentOS_Repo/master/Docker/docker-ce.repo
sudo yum-config-manager --add-repo /opt/docker-ce.repo

# 更新yum缓存
echo ""
echo "= = = = = = = = = 更新yum缓存 = = = = = = = = ="
echo ""
sudo yum makecache fast

# 查看所有仓库中所有docker版本，并选择特定版本安装
echo ""
echo "= = = = = = = = = 查看所有仓库中所有docker版本，并选择特定版本安装 = = = = = = = = ="
echo ""
sudo yum list docker-ce --showduplicates | sort -r

# 安装 docker-ce docker-ce-cli containerd.io
echo ""
echo "= = = = = = = = = 安装 docker-ce docker-ce-cli containerd.io = = = = = = = = ="
echo ""
sudo yum -y install docker-ce docker-ce-cli containerd.io

# 启动Docker Start Docker
echo ""
echo "= = = = = = = = = 启动Docker Start Docker = = = = = = = = ="
echo ""
sudo systemctl start docker

# 验证安装是否成功
echo ""
echo "= = = = = = = = = 验证安装是否成功 = = = = = = = = ="
echo ""
docker version

# 卸载Docker
# sudo yum -y remove docker-ce docker-ce-cli containerd.io && sudo rm -rf /var/lib/docker
# sudo yum -y remove yum-utils device-mapper-persistent-data lvm2
```

### 3.2 执行结果

``` bash
[ryanjie@Ryanjie Docker]$ chmod +x GetDocker.sh && sudo sh GetDocker.sh

= = = = = = = = = Docker 要求 CentOS 系统的内核版本高于 3.10,下面来验证你的CentOS 版本是否支持 Docker = = = = = = = = =

3.10.0-862.9.1.el7.x86_64

= = = = = = = = = 卸载旧版本 Uninstall old version = = = = = = = = =

Loaded plugins: fastestmirror
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
No Match for argument: docker
No Match for argument: docker-client
No Match for argument: docker-client-latest
No Match for argument: docker-common
No Match for argument: docker-latest
No Match for argument: docker-latest-logrotate
No Match for argument: docker-logrotate
No Match for argument: docker-engine
No Packages marked for removal

= = = = = = = = = 安装依赖包 Install required packages = = = = = = = = =

Loaded plugins: fastestmirror
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Loading mirror speeds from cached hostfile
Resolving Dependencies
--> Running transaction check
---> Package device-mapper-persistent-data.x86_64 0:0.7.3-3.el7 will be installed
---> Package lvm2.x86_64 7:2.02.180-10.el7_6.3 will be installed
---> Package yum-utils.noarch 0:1.1.31-50.el7 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

===========================================================================================
 Package                           Arch       Version                    Repository   Size
===========================================================================================
Installing:
 device-mapper-persistent-data     x86_64     0.7.3-3.el7                base        405 k
 lvm2                              x86_64     7:2.02.180-10.el7_6.3      updates     1.3 M
 yum-utils                         noarch     1.1.31-50.el7              base        121 k

Transaction Summary
===========================================================================================
Install  3 Packages

Total download size: 1.8 M
Installed size: 4.5 M
Downloading packages:
(1/3): yum-utils-1.1.31-50.el7.noarch.rpm                           | 121 kB  00:00:00     
(2/3): device-mapper-persistent-data-0.7.3-3.el7.x86_64.rpm         | 405 kB  00:00:00     
(3/3): lvm2-2.02.180-10.el7_6.3.x86_64.rpm                          | 1.3 MB  00:00:00     
-------------------------------------------------------------------------------------------
Total                                                      2.9 MB/s | 1.8 MB  00:00:00     
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : device-mapper-persistent-data-0.7.3-3.el7.x86_64                        1/3 
  Installing : 7:lvm2-2.02.180-10.el7_6.3.x86_64                                       2/3 
  Installing : yum-utils-1.1.31-50.el7.noarch                                          3/3 
  Verifying  : device-mapper-persistent-data-0.7.3-3.el7.x86_64                        1/3 
  Verifying  : 7:lvm2-2.02.180-10.el7_6.3.x86_64                                       2/3 
  Verifying  : yum-utils-1.1.31-50.el7.noarch                                          3/3 

Installed:
  device-mapper-persistent-data.x86_64 0:0.7.3-3.el7   lvm2.x86_64 7:2.02.180-10.el7_6.3  
  yum-utils.noarch 0:1.1.31-50.el7                    

Complete!

= = = = = = = = = 设置软件yum源 = = = = = = = = =

--2019-04-13 19:25:02--  https://raw.githubusercontent.com/Ryanjiena/CentOS_Repo/master/Docker/docker-ce.repo
Resolving raw.githubusercontent.com (raw.githubusercontent.com)... 151.101.76.133
Connecting to raw.githubusercontent.com (raw.githubusercontent.com)|151.101.76.133|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 2880 (2.8K) [text/plain]
Saving to: ‘/opt/docker-ce.repo’

100%[=================================================>] 2,880       --.-K/s   in 0s      

2019-04-13 19:25:04 (21.4 MB/s) - ‘/opt/docker-ce.repo’ saved [2880/2880]

Loaded plugins: fastestmirror
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
adding repo from: /opt/docker-ce.repo
grabbing file /opt/docker-ce.repo to /etc/yum.repos.d/docker-ce.repo
repo saved to /etc/yum.repos.d/docker-ce.repo

= = = = = = = = = 更新yum缓存 = = = = = = = = =

Loaded plugins: fastestmirror
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Loading mirror speeds from cached hostfile
base                                                                | 3.6 kB  00:00:00     
docker-ce-stable                                                    | 3.5 kB  00:00:00     
epel                                                                | 4.7 kB  00:00:00     
extras                                                              | 3.4 kB  00:00:00     
updates                                                             | 3.4 kB  00:00:00     
Metadata Cache Created

= = = = = = = = = 查看所有仓库中所有docker版本，并选择特定版本安装 = = = = = = = = =

Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Loading mirror speeds from cached hostfile
Loaded plugins: fastestmirror
docker-ce.x86_64            3:18.09.5-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.4-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.3-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.2-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.1-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.0-3.el7                     docker-ce-stable
docker-ce.x86_64            18.06.3.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.06.2.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.06.1.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.06.0.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.03.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            18.03.0.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.12.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.12.0.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.09.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.09.0.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.06.2.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.06.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.06.0.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.03.3.ce-1.el7                    docker-ce-stable
docker-ce.x86_64            17.03.2.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.03.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.03.0.ce-1.el7.centos             docker-ce-stable
Available Packages

= = = = = = = = = 安装 docker-ce docker-ce-cli containerd.io = = = = = = = = =

Loaded plugins: fastestmirror
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Repository base is listed more than once in the configuration
Repository updates is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
Loading mirror speeds from cached hostfile
Resolving Dependencies
--> Running transaction check
---> Package containerd.io.x86_64 0:1.2.5-3.1.el7 will be installed
---> Package docker-ce.x86_64 3:18.09.5-3.el7 will be installed
---> Package docker-ce-cli.x86_64 1:18.09.5-3.el7 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

===========================================================================================
 Package              Arch          Version                  Repository               Size
===========================================================================================
Installing:
 containerd.io        x86_64        1.2.5-3.1.el7            docker-ce-stable         22 M
 docker-ce            x86_64        3:18.09.5-3.el7          docker-ce-stable         19 M
 docker-ce-cli        x86_64        1:18.09.5-3.el7          docker-ce-stable         14 M

Transaction Summary
===========================================================================================
Install  3 Packages

Total download size: 55 M
Installed size: 236 M
Downloading packages:
(1/3): containerd.io-1.2.5-3.1.el7.x86_64.rpm                       |  22 MB  00:00:06     
(2/3): docker-ce-18.09.5-3.el7.x86_64.rpm                           |  19 MB  00:00:10     
(3/3): docker-ce-cli-18.09.5-3.el7.x86_64.rpm                       |  14 MB  00:00:04     
-------------------------------------------------------------------------------------------
Total                                                      4.8 MB/s |  55 MB  00:00:11     
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : containerd.io-1.2.5-3.1.el7.x86_64                                      1/3 
  Installing : 1:docker-ce-cli-18.09.5-3.el7.x86_64                                    2/3 
  Installing : 3:docker-ce-18.09.5-3.el7.x86_64                                        3/3 
  Verifying  : 1:docker-ce-cli-18.09.5-3.el7.x86_64                                    1/3 
  Verifying  : 3:docker-ce-18.09.5-3.el7.x86_64                                        2/3 
  Verifying  : containerd.io-1.2.5-3.1.el7.x86_64                                      3/3 

Installed:
  containerd.io.x86_64 0:1.2.5-3.1.el7           docker-ce.x86_64 3:18.09.5-3.el7          
  docker-ce-cli.x86_64 1:18.09.5-3.el7          

Complete!

= = = = = = = = = 启动Docker Start Docker = = = = = = = = =


= = = = = = = = = 验证安装是否成功 = = = = = = = = =

Client:
 Version:           18.09.5
 API version:       1.39
 Go version:        go1.10.8
 Git commit:        e8ff056
 Built:             Thu Apr 11 04:43:34 2019
 OS/Arch:           linux/amd64
 Experimental:      false

Server: Docker Engine - Community
 Engine:
  Version:          18.09.5
  API version:      1.39 (minimum version 1.12)
  Go version:       go1.10.8
  Git commit:       e8ff056
  Built:            Thu Apr 11 04:13:40 2019
  OS/Arch:          linux/amd64
  Experimental:     false
[ryanjie@Ryanjie Docker]$ 
```
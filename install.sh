    yum -q -y clean all
    rm -rf /var/cache/yum
    yum -q -y makecache all
    mkdir -p ~/bin
    yum -q -y install ntfs-3g fuse-exfat exfat-utils man-pages deltarpm
    yum -q -y group install x11 xfce
    systemctl set-default graphical.target
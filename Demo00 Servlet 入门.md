---
title: Demo00 Servlet 入门
date: 2018-09-07 09:25:00
author: Ryanjie
img: https://wx3.sinaimg.cn/large/006pYIPbly1g1rrzuhwqjj325s0yie85.jpg
top: false
cover: false
toc: true
mathjax: false
summary: Demo00 Java环境搭建：配置Java环境变量、安装和配置Eclipse和常见问题。
categories: Java入坑之旅
tags:
  - Java
  - 入坑之旅
---

了解 B/S (Browser/Server)模式
掌握 Servlet 开发技巧
掌握 Servlet 执行原理

请求和响应

 - 从浏览器发送给服务器的数据包称为"`请求(Request)`"
 - 从服务器返回给浏览器的结果称为"`响应(Response)`"

J2EE

 - J2EE(Java 2 Platform Enterprise Editor) 是指 "Java 2 企业版"
 - 开发BS(Web)应用程序是J2EE最核心的功能
 - J2EE有13个模块组成


J2EE 中的13个功能模块

 - Servlet: web服务器小程序
 - JSP: 服务器页面
 - JDBC: 数据库交互模块
 - XML: XML交互模块
 - EJB: 企业级Java Bean
 - RMI: 远程调用
 - JNDI:目录服务
 - JMS: 消息服务
 - JTA: 事务管理
 - JavaMail: 发送/接受 Email
 - JAF: 安全框架
 - CORBA: CORBA集成
 - JTS: CORBA事务监控
 
Apache Tomcat

 - Tomcat 是 Apache 软件基金会旗下一款免费的开放源代码的 Web 应用服务器程序.

J2EE 与 Tomcat 的关系

 - `J2EE`是一组技术规范与指南,具体实现由软件厂商决定
 - `Tomcat` 是 `J2EE Web(Servlet与JSP)` 标准的实现者
 - `J2SE(JDK)` 是 `J2EE` 运行的基石, 运行 `Tomcat`离不开 `J2SE`. 

Servlet

 - Servlet(Servlet Applet) 服务器小程序,主要功能用于生成的动态Web内容
 - Servlet 是J2EE最重要的组成部分


# 0x01. 

-----------------

> **版权声明：** 本博客所有文章除特别声明外，均采用 **[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)** 许可协议。
> 
> **作者**：[@Ryanjie](https://www.ryanjie.xyz)  ||  [@Ryanjie's Cnblogs](https://www.cnblogs.com/Ryanjie)  ||  [@Ryanjie's Github](https://github.com/ryanjiena)  ||  [@Ryanjie's Gitee](https://gitee.com/ryanjiena)
> 
>  **转载请注明出处：**[@Ryanjie](https://www.ryanjie.xyz)

------